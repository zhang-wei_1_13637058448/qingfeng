<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>国际化</title>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.9.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.cookie.js"></script>
</head>
<body>
<div align="center" style="margin: 20px auto">
    <h1><spring:message code='login.loginName'/><br></h1>
    <h1><spring:message code='login.loginPwd'/><br></h1>
    <h1 style="color: red"><spring:message code='qingfeng.title'/><br></h1>
    <div style="font-size: 24px">
        <a href="javascript:;" onclick="changeLang('zh')"> 中文 </a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="javascript:;" onclick="changeLang('en')"> english </a>
    </div>
</div>
</body>
<script>
    function changeLang(lang) {
        //cookie持久化
        $.cookie('ClientLanguage',lang);
        location.reload();
    }
</script>
</html>