package com.qingfeng.framework.servlet;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * @ProjectName Customlister
 * @author qingfeng
 * @version 1.0.0
 * @Description Customlister
 * @createTime 2021/12/30 0030 21:22
 */
//@WebListener
public class Customlister implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        System.out.println("1");
    }
    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println("2");
    }
}