package com.qingfeng.framework.servlet;

import com.qingfeng.util.upload.ParaUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @ProjectName WebMvcConfigurer
 * @author qingfeng
 * @version 1.0.0
 * @Description 拦截器
 * @createTime 2021/12/30 0030 21:23
 */
@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**")
                .addResourceLocations("file:" + ParaUtil.localName);
        super.addResourceHandlers(registry);
    }

}
