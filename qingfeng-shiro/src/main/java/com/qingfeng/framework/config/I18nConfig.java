package com.qingfeng.framework.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;

/**
 * @ProjectName I18nConfig
 * @author qingfeng
 * @version 1.0.0
 * @Description 设置默认语言
 * @createTime 2021/7/22 0022 21:03
 */
@Configuration
public class I18nConfig {

    private static Logger logger = LoggerFactory.getLogger(I18nConfig.class);

    @Bean(name = "localeResolver")
    public LocaleResolver localeResolver() {
        logger.info("创建cookieLocaleResolver");

        LocaleResolver localeResolver = new LocaleResolver();
        localeResolver.setDefaultLocale(Locale.CHINA);
        logger.info("cookieLocaleResolver:");
        return localeResolver;
    }

}