package com.qingfeng.framework.config;

import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @ProjectName LocaleResolver
 * @author qingfeng
 * @version 1.0.0
 * @Description 每次加载页面时都会被调用，加载页面时都会设置并修改语言
 * @createTime 2021/7/22 0022 21:03
 */
public class LocaleResolver extends AcceptHeaderLocaleResolver {

    private Locale myLocal;

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        //获取cookie数组
        Cookie[] cookies = request.getCookies();
        String lang = "";
        if (cookies != null) {
            //遍历cookie数组
            for (Cookie cookie : cookies) {
                if ("ClientLanguage".equals(cookie.getName())) {
                    lang = cookie.getValue();
                    break;
                }
            }
        }
        LocaleResolver localeResolver = (LocaleResolver) RequestContextUtils.getLocaleResolver(request);
        if (lang == null || "".equals(lang)) {
            myLocal = Locale.CHINA;
        } else {
            if (lang.equals("zh")) {
                myLocal = Locale.CHINA;
            } else if (lang.equals("en")) {
                myLocal = Locale.US;
            }
        }
        return myLocal;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        myLocal = locale;
    }
}