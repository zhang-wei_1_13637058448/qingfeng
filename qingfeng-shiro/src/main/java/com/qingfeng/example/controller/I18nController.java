package com.qingfeng.example.controller;

import com.qingfeng.base.controller.BaseController;
import com.qingfeng.util.PageData;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ProjectName I18nController
 * @author qingfeng
 * @version 1.0.0
 * @Description I18nController
 * @createTime 2021/7/22 0022 21:10
 */
@Controller
@RequestMapping(value = "/example/i18n")
public class I18nController extends BaseController {

	/**
	 * @title index
	 * @description index
	 * @author qingfeng
	 * @updateTime 2021/7/22 0022 21:10
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
		public String index(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException {
		PageData pd = new PageData(request);
		map.put("pd",pd);
		return "web/example/i18n/index";
	}


}
