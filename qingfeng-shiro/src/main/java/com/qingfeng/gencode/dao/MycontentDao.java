package com.qingfeng.gencode.dao;

import com.qingfeng.base.dao.CrudDao;
import com.qingfeng.util.PageData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Title: MycontentDao
 * @ProjectName qingfeng
 * @Description: MycontentDao
 * @author qingfeng
 * @date 2020-9-22 22:42
 */
@Mapper
public interface MycontentDao extends CrudDao<PageData> {

    /**
    * @Description: updateStatus
    * @Param: [pd]
    * @return: void
    * @Author: qingfeng
    * @Date: 2020-10-13 11:10
    */
    public void updateStatus(PageData pd);




}
