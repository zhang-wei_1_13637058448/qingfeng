package com.qingfeng.quartz.dao;

import com.qingfeng.base.dao.CrudDao;
import com.qingfeng.util.PageData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Title: TimTaskDao
 * @ProjectName qingfeng
 * @Description: TODO
 * @author qingfeng
 * @date 2020-10-1 21:53
 */
@Mapper
public interface TimTaskDao extends CrudDao<PageData> {

    

}
