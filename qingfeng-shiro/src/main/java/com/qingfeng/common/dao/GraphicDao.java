package com.qingfeng.common.dao;

import com.qingfeng.base.dao.CrudDao;
import com.qingfeng.util.PageData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Title: GraphicDao
 * @ProjectName qingfeng
 * @Description: GraphicDao类
 * @author qingfeng
 * @date 2020-10-8 20:21
 */
@Mapper
public interface GraphicDao extends CrudDao<PageData> {

}
