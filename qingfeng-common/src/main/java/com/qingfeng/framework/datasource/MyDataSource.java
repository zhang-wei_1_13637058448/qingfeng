package com.qingfeng.framework.datasource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ProjectName MyDataSource
 * @author qingfeng
 * @version 1.0.0
 * @Description 数据源选择--自定义注解
 * @createTime 2021/12/30 0030 21:16
 */
@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.METHOD) 
public @interface MyDataSource {

	DataSourceType value() default DataSourceType.Master;	//默认主表
	
}
