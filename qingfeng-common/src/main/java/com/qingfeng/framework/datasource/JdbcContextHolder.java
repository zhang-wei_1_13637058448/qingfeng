package com.qingfeng.framework.datasource;
/**
 * @ProjectName JdbcContextHolder
 * @author qingfeng
 * @version 1.0.0
 * @Description 动态数据源的上下文 threadlocal
 * @createTime 2021/12/30 0030 21:16
 */
public class JdbcContextHolder {
	
	private final static ThreadLocal<String> local = new ThreadLocal<>();
	
	public static void putDataSource(String name) {
		local.set(name);
	}
	
	public static String getDataSource() {
		return local.get();
	}
	
}