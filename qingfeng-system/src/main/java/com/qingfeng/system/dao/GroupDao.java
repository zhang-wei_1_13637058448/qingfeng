package com.qingfeng.system.dao;

import com.qingfeng.base.dao.CrudDao;
import com.qingfeng.util.PageData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Title: GroupDao
 * @ProjectName qingfeng
 * @Description: 用户组DAO层
 * @author qingfeng
 * @date 2020-9-22 22:42
 */
@Mapper
public interface GroupDao extends CrudDao<PageData> {

    /** 
     * @Description: saveGroupUser 
     * @Param: [pd] 
     * @return: void 
     * @Author: qingfeng
     * @Date: 2020-9-25 23:04 
     */ 
    public void saveGroupUser(PageData pd);

    /** 
     * @Description: findGroupUser 
     * @Param: [pd] 
     * @return: java.util.List<com.qingfeng.util.PageData> 
     * @Author: qingfeng
     * @Date: 2020-9-25 23:04 
     */ 
    public List<PageData> findGroupUser(PageData pd);

    /** 
     * @Description: delGroupUser 
     * @Param: [pd] 
     * @return: void 
     * @Author: qingfeng
     * @Date: 2020-9-25 23:04 
     */ 
    public void delGroupUser(PageData pd);

    /** 
     * @Description: updateGroupUser
     * @Param: [pd] 
     * @return: void 
     * @Author: qingfeng
     * @Date: 2020-9-25 23:08
     */ 
    public void updateGroupUser(PageData pd);

}
